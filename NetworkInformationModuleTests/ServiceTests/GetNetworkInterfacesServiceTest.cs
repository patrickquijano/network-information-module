﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using NetworkInformationModule.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class GetNetworkInterfacesServiceTest : TestBase
    {
        private IGetNetworkInterfacesService _GetNetworkInterfacesService;

        [TestInitialize]
        public void Setup()
        {
            this._GetNetworkInterfacesService = this.Resolve<IGetNetworkInterfacesService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void GetNetworkInterfaces()
        {
            var networkInterfaces = this._GetNetworkInterfacesService.Get();
            Debug.WriteLine(this.BuildOutput(networkInterfaces));

            Assert.IsNotNull(networkInterfaces);
        }

        [TestMethod]
        public void GetNetworkInterfacesOnlyNonVirtual()
        {
            var networkInterfaces = this._GetNetworkInterfacesService.Get(true);
            Debug.WriteLine(this.BuildOutput(networkInterfaces));

            Assert.IsNotNull(networkInterfaces);
        }

        [TestMethod]
        public async Task GetNetworkInterfacesAsync()
        {
            var networkInterfaces = await this._GetNetworkInterfacesService.GetAsync();
            Debug.WriteLine(this.BuildOutput(networkInterfaces));

            Assert.IsNotNull(networkInterfaces);
        }

        [TestMethod]
        public async Task GetNetworkInterfacesServiceAsyncOnlyNonVirtual()
        {
            var networkInterfaces = await this._GetNetworkInterfacesService.GetAsync(true);
            Debug.WriteLine(this.BuildOutput(networkInterfaces));

            Assert.IsNotNull(networkInterfaces);
        }

        private string BuildOutput(NetworkInterfaces networkInterfaces)
        {
            var builder = new StringBuilder();
            foreach (var networkInterface in networkInterfaces.Data)
            {
                builder.AppendLine(new string('=', 100));
                builder.AppendLine($"Description: {networkInterface.Description}");
                builder.AppendLine($"Interface Type: {networkInterface.NetworkInterfaceType}");
                builder.AppendLine($"Physical Address: {networkInterface.PhysicalAddress}");
                builder.AppendLine($"Operational Status: {networkInterface.OperationalStatus}");
                var ipVersions = new List<string>();
                if (networkInterface.SupportsIPv4)
                {
                    ipVersions.Add("IPv4");
                }
                if (networkInterface.SupportsIPv6)
                {
                    ipVersions.Add("IPv6");
                }
                builder.AppendLine($"IP Version: {string.Join(' ', ipVersions)}");
                builder.AppendLine($"DNS Suffix: {networkInterface.IPInterfaceProperties.DnsSuffix}");
                if (networkInterface.SupportsIPv4)
                {
                    builder.AppendLine($"MTU: {networkInterface.IPv4InterfaceProperties.Mtu}");
                }
                builder.AppendLine($"DNS Enabled: {networkInterface.IPInterfaceProperties.IsDnsEnabled}");
                builder.AppendLine($"Dynamic Enabled: {networkInterface.IPInterfaceProperties.IsDynamicDnsEnabled}");
                builder.AppendLine($"Receive Only: {networkInterface.IsReceiveOnly}");
                builder.AppendLine($"Supports Multicast: {networkInterface.SupportsMulticast}");
                builder.AppendLine(new string('=', 100));
            }

            return builder.ToString();
        }
    }
}
