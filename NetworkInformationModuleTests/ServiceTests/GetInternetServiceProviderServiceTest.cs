﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class GetInternetServiceProviderServiceTest : TestBase
    {
        private IGetInternetServiceProviderService _GetInternetServiceProviderService;

        [TestInitialize]
        public void Setup()
        {
            this._GetInternetServiceProviderService = this.Resolve<IGetInternetServiceProviderService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void GetInternetServiceProvider()
        {
            var internetServiceProvider = this._GetInternetServiceProviderService.Get();
            Debug.WriteLine(internetServiceProvider);

            Assert.IsNotNull(internetServiceProvider);
        }

        [TestMethod]
        public async Task GetInternetServiceProviderAsync()
        {
            var internetServiceProvider = await this._GetInternetServiceProviderService.GetAsync();
            Debug.WriteLine(internetServiceProvider);

            Assert.IsNotNull(internetServiceProvider);
        }
    }
}
