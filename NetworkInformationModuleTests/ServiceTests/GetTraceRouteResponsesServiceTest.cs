﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class GetTraceRouteResponsesServiceTest : TestBase
    {
        private IGetTraceRouteResponsesService _GetTraceRouteResponsesService;

        [TestInitialize]
        public void Setup()
        {
            this._GetTraceRouteResponsesService = this.Resolve<IGetTraceRouteResponsesService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void GetTraceRouteResponses()
        {
            var traceRouteResponses = this._GetTraceRouteResponsesService.Get();
            Debug.WriteLine(traceRouteResponses);

            Assert.IsNotNull(traceRouteResponses);
        }

        [TestMethod]
        public void GetTraceRouteResponsesCustomRoundTrips()
        {
            var traceRouteResponses = this._GetTraceRouteResponsesService.Get(10);
            Debug.WriteLine(traceRouteResponses);

            Assert.IsNotNull(traceRouteResponses);
        }

        [TestMethod]
        public void GetTraceRouteResponsesFacebook()
        {
            var traceRouteResponses = this._GetTraceRouteResponsesService.Get("www.facebook.com");
            Debug.WriteLine(traceRouteResponses);

            Assert.IsNotNull(traceRouteResponses);
        }

        [TestMethod]
        public void GetTraceRouteResponsesGoogle()
        {
            var traceRouteResponses = this._GetTraceRouteResponsesService.Get("www.google.com");
            Debug.WriteLine(traceRouteResponses);

            Assert.IsNotNull(traceRouteResponses);
        }

        [TestMethod]
        public async Task GetTraceRouteResponsesAsync()
        {
            var traceRoutes = await this._GetTraceRouteResponsesService.GetAsync();
            Debug.WriteLine(traceRoutes);

            Assert.IsNotNull(traceRoutes);
        }

        [TestMethod]
        public async Task GetTraceRouteResponsesAsyncCustomRoundTrips()
        {
            var traceRoutes = await this._GetTraceRouteResponsesService.GetAsync(10);
            Debug.WriteLine(traceRoutes);

            Assert.IsNotNull(traceRoutes);
        }

        [TestMethod]
        public async Task GetTraceRouteResponsesAsyncFacebook()
        {
            var traceRoutes = await this._GetTraceRouteResponsesService.GetAsync("www.facebook.com");
            Debug.WriteLine(traceRoutes);

            Assert.IsNotNull(traceRoutes);
        }

        [TestMethod]
        public async Task GetTraceRouteResponsesAsyncFacebookCustomRoundTrips()
        {
            var traceRoutes = await this._GetTraceRouteResponsesService.GetAsync("www.facebook.com", 10);
            Debug.WriteLine(traceRoutes);

            Assert.IsNotNull(traceRoutes);
        }

        [TestMethod]
        public async Task GetTraceRouteResponsesAsyncGoogle()
        {
            var traceRoutes = await this._GetTraceRouteResponsesService.GetAsync("www.google.com");
            Debug.WriteLine(traceRoutes);

            Assert.IsNotNull(traceRoutes);
        }

        [TestMethod]
        public async Task GetTraceRouteResponsesAsyncGoogleCustomRoundTrips()
        {
            var traceRoutes = await this._GetTraceRouteResponsesService.GetAsync("www.google.com", 10);
            Debug.WriteLine(traceRoutes);

            Assert.IsNotNull(traceRoutes);
        }
    }
}
