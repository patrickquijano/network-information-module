﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using System.Diagnostics;
using System.Threading;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class GetThroughputServiceTest : TestBase
    {
        private IEnqueueNetworkPerformanceService _EnqueueNetworkPerformanceService;
        private IGetThroughputService _GetThroughputService;

        [TestInitialize]
        public void Setup()
        {
            this._EnqueueNetworkPerformanceService = this.Resolve<IEnqueueNetworkPerformanceService>();
            this._GetThroughputService = this.Resolve<IGetThroughputService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void GetThroughput()
        {
            for (int i = 0; i < 10; i++)
            {
                this._EnqueueNetworkPerformanceService.Enqueue();
                Thread.Sleep(1000);
            }
            var throughput = this._GetThroughputService.Get();
            Debug.WriteLine(throughput);

            Assert.IsNotNull(throughput);
        }
    }
}
