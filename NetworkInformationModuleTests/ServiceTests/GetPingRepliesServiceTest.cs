﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using NetworkInformationModule.Models;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class GetPingRepliesServiceTest : TestBase
    {
        private IGetPingRepliesService _GetPingRepliesService;

        [TestInitialize]
        public void Setup()
        {
            this._GetPingRepliesService = this.Resolve<IGetPingRepliesService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void GetPingReplies()
        {
            var pingReplies = this._GetPingRepliesService.Get();
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public void GetPingRepliesCustomRoundTrips()
        {
            var pingReplies = this._GetPingRepliesService.Get(10);
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public void GetPingRepliesFacebook()
        {
            var pingReplies = this._GetPingRepliesService.Get("www.facebook.com");
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public void GetPingRepliesGoogle()
        {
            var pingReplies = this._GetPingRepliesService.Get("www.google.com");
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public async Task GetPingRepliesAsync()
        {
            var pingReplies = await this._GetPingRepliesService.GetAsync();
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public async Task GetPingRepliesAsyncCustomRoundTrips()
        {
            var pingReplies = await this._GetPingRepliesService.GetAsync(10);
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public async Task GetPingRepliesFacebookAsync()
        {
            var pingReplies = await this._GetPingRepliesService.GetAsync("www.facebook.com");
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public async Task GetPingRepliesGoogleAsync()
        {
            var pingReplies = await this._GetPingRepliesService.GetAsync("www.google.com");
            Debug.WriteLine(pingReplies);

            Assert.IsNotNull(pingReplies);
        }

        [TestMethod]
        public async Task GetPingRepliesInvalidHostName()
        {
            var pingReplies = (PingReplies)null;
            try
            {
                pingReplies = await this._GetPingRepliesService.GetAsync("!@#$%^&*()");
            }
            catch { };

            Assert.IsNull(pingReplies);
        }
    }
}
