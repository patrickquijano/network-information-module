﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class GetPublicIPAddressServiceTest : TestBase
    {
        private IGetPublicIPAddressService _GetPublicIPAddressService;

        [TestInitialize]
        public void Setup()
        {
            this._GetPublicIPAddressService = this.Resolve<IGetPublicIPAddressService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void GetPublicIPAddress()
        {
            var publicIPAddress = this._GetPublicIPAddressService.Get();
            Debug.WriteLine(publicIPAddress);

            Assert.IsNotNull(publicIPAddress);
        }

        [TestMethod]
        public async Task GetPublicIPAddressAsync()
        {
            var publicIPAddress = await this._GetPublicIPAddressService.GetAsync();
            Debug.WriteLine(publicIPAddress);

            Assert.IsNotNull(publicIPAddress);
        }
    }
}
