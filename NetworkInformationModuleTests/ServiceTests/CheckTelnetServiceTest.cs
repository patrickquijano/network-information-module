﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkInformationModule.IServices;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NetworkInformationModuleTests.ServiceTests
{
    [TestClass]
    public class CheckTelnetServiceTest : TestBase
    {
        private ICheckTelnetService _CheckTelnetService;

        [TestInitialize]
        public void Setup()
        {
            this._CheckTelnetService = this.Resolve<ICheckTelnetService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void CheckTelnet()
        {
            var telnetResult = this._CheckTelnetService.Check(443, message => Debug.WriteLine(message));

            Assert.IsTrue(telnetResult);
        }

        [TestMethod]
        public async Task CheckTelnetAsync()
        {
            var telnetResult = await this._CheckTelnetService.CheckAsync(443, message => Debug.WriteLine(message));

            Assert.IsTrue(telnetResult);
        }
    }
}
