﻿using System.Net.NetworkInformation;

namespace NetworkInformationModule.Models
{
    public class NetworkInterface
    {
        public OperationalStatus OperationalStatus { get; set; }
        public NetworkInterfaceType NetworkInterfaceType { get; set; }
        public bool IsWired => this.NetworkInterfaceType == NetworkInterfaceType.Ethernet;
        public bool IsWireless => this.NetworkInterfaceType == NetworkInterfaceType.Wireless80211;
        public string Name { get; set; }
        public bool IsReceiveOnly { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public long Speed { get; set; }
        public double SpeedInKilobits => this.Speed / 1000D;
        public double SpeedInMegabits => this.SpeedInKilobits / 1000D;
        public bool SupportsMulticast { get; set; }
        public IPInterfaceProperties IPInterfaceProperties { get; set; }
        public IPv4InterfaceProperties IPv4InterfaceProperties { get; set; }
        public IPv6InterfaceProperties IPv6InterfaceProperties { get; set; }
        public IPInterfaceStatistics IPInterfaceStatistics { get; set; }
        public IPv4InterfaceStatistics IPv4InterfaceStatistics { get; set; }
        public PhysicalAddress PhysicalAddress { get; set; }
        public bool SupportsIPv4 { get; set; }
        public bool SupportsIPv6 { get; set; }
    }
}
