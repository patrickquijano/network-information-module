﻿using NetworkInformationModule.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace NetworkInformationModule.Models
{
    public class TraceRouteResponses
    {
        public IEnumerable<TraceRouteResponse> Data { get; set; }
        public IPAddress Address { get; set; }
        public string HostName { get; set; }
        public string HostNameAndOrAddress => !this.HostName.IsNullOrWhiteSpace() ? $"{this.HostName} [{this.Address}]" : $"{this.Address}";
        public int MaxHops { get; set; }
        public TraceRouteResponse HighestAveRoundTripTimeTraceRouteResponse => this.Data.Select(traceRouteResponse => traceRouteResponse)
            .OrderByDescending(traceRouteResponse => traceRouteResponse.PingReplies.AveRoundTripTime)
            .FirstOrDefault();
        public TraceRouteResponse HighestPacketLossTraceRouteResponse => this.Data.Select(traceRouteResponse => traceRouteResponse)
            .Where(traceRouteResponse => traceRouteResponse.PingReplies.PacketsLost > 0)
            .OrderByDescending(traceRouteResponse => traceRouteResponse.PingReplies.PacketLoss)
            .FirstOrDefault();

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Tracing route to {this.HostNameAndOrAddress} over a maximum of {this.MaxHops} hops");
            builder.AppendLine();
            this.Data.ForEach(traceRouteResponse => builder.AppendLine($"{traceRouteResponse}"));
            builder.AppendLine();
            builder.AppendLine("Trace complete.");

            return builder.ToString();
        }
    }
}
