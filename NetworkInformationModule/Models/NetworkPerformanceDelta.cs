﻿namespace NetworkInformationModule.Models
{
    public class NetworkPerformanceDelta
    {
        public long BytesReceivedDelta { get; set; }
        public long BitsReceivedDelta => this.BytesReceivedDelta * 8;
        public long BytesSentDelta { get; set; }
        public long BitsSentDelta => this.BytesSentDelta * 8;
        public long Speed { get; set; }
        public double SpeedInKilobits => this.Speed / 1000D;
        public double SpeedInMegabits => this.SpeedInKilobits / 1000D;
        public long BytesTransferred => this.BytesReceivedDelta + this.BytesSentDelta;
        public long BitsTransferred => this.BytesTransferred * 8;
        public long Preamble => 8 * 8;
        public long InterFrameGap => 12 * 8;
        public long WindowFrameSize => 64 * 8;
        public long FrameSize => this.Preamble + this.InterFrameGap + this.WindowFrameSize;
        public double FramesPerSecond => this.BitsTransferred / this.FrameSize;
        public double MaxTheoreticalThroughput => this.FramesPerSecond * this.WindowFrameSize;
        public double MaxTheoreticalThroughputInKilobits => this.MaxTheoreticalThroughput / 1000D;
        public double MaxTheoreticalThroughputInMegabits => this.MaxTheoreticalThroughputInKilobits / 1000D;
        public double MaxActualThroughput => this.MaxTheoreticalThroughput - (this.FramesPerSecond * this.InterFrameGap) - (this.FramesPerSecond * this.Preamble);
        public double MaxActualThroughputInKilobits => this.MaxActualThroughput / 1000D;
        public double MaxActualThroughputInMegabits => this.MaxActualThroughputInKilobits / 1000D;
    }
}
