﻿using System.Net;
using System.Net.NetworkInformation;

namespace NetworkInformationModule.Models
{
    public class PingReply
    {
        public IPAddress Address { get; set; }
        public byte[] Buffer { get; set; }
        public PingOptions Options { get; set; }
        public long RoundtripTime { get; set; }
        public IPStatus Status { get; set; }
        public string HostName { get; set; }

        public override string ToString()
        {
            switch (this.Status)
            {
                case IPStatus.DestinationHostUnreachable:
                    return $"Reply from {this.Address}: Destination host unreachable.";
                case IPStatus.Success:
                    return $"Reply from {this.Address}: bytes={this.Buffer.Length} time={this.RoundtripTime}ms TTL={this.Options.Ttl}";
                case IPStatus.TtlExpired:
                    return $"Reply from {this.Address}: TTL Expired In Transit.";
                case IPStatus.Unknown:
                    return "Unknown host.";
                default:
                    return "Request timed out.";
            }
        }
    }
}
