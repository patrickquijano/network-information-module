﻿namespace NetworkInformationModule.Models
{
    public class NetworkPerformance
    {
        public long BytesReceived { get; set; }
        public long BitsReceived => this.BytesReceived * 8;
        public long BytesSent { get; set; }
        public long BitsSent => this.BytesSent * 8;
        public long Speed { get; set; }
        public double SpeedInKilobits => this.Speed / 1000D;
        public double SpeedInMegabits => this.SpeedInKilobits / 1000D;
    }
}
