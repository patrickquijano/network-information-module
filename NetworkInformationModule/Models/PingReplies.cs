﻿using NetworkInformationModule.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace NetworkInformationModule.Models
{
    public class PingReplies
    {
        public IEnumerable<PingReply> Data { get; set; }
        public IPAddress Address => this.Data.FirstOrDefault(pingReply => pingReply.Status != IPStatus.TimedOut)?.Address;
        public string HostName => this.Data.FirstOrDefault(pingReply => pingReply.Status != IPStatus.TimedOut)?.HostName;
        public byte[] Buffer => this.Data.FirstOrDefault(pingReply => pingReply.Status != IPStatus.TimedOut)?.Buffer;
        public double PacketsSent => this.Data.Count();
        public int PacketsReceived => this.Data.Count(pingReply => pingReply.Status != IPStatus.TimedOut);
        public int PacketsLost => this.Data.Count(pingReply => pingReply.Status == IPStatus.TimedOut);
        public double PacketLoss => this.PacketsLost / this.PacketsSent;
        public long MinRoundTripTime => this.Data.Min(pingReply => pingReply.RoundtripTime);
        public long MaxRoundTripTime => this.Data.Max(pingReply => pingReply.RoundtripTime);
        public double AveRoundTripTime => this.Data.Average(pingReply => pingReply.RoundtripTime);
        public double AveJitter => this.Data.Where(pingReply => pingReply.Status == IPStatus.Success)
            .Select(pingReply => pingReply.RoundtripTime)
            .SelectWithPrevious((previous, current) => Math.Abs(previous - current))
            .Average();
        public PingReply BestPingReply => this.Data.Where(pingReply => pingReply.Status != IPStatus.TimedOut)
            .OrderBy(pingReply => pingReply.RoundtripTime)
            .FirstOrDefault();
        public PingReply WorstPingReply => this.Data.Where(pingReply => pingReply.Status != IPStatus.TimedOut)
            .OrderByDescending(pingReply => pingReply.RoundtripTime)
            .FirstOrDefault();
        public double EffectiveLatency => this.AveRoundTripTime + this.AveJitter * 2 + 10;
        public double MeanOpinionScore
        {
            get
            {
                var r = 93.2 - (this.EffectiveLatency < 160 ? (this.EffectiveLatency / 40) : (this.EffectiveLatency - 120) / 10);
                r -= (this.PacketLoss * 2.5);

                return 1 + 0.035 * r + 0.000007 * r * (r - 60) * (100 - r);
            }
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Pinging {(!this.HostName.IsNullOrWhiteSpace() ? $"{this.HostName} [{this.Address}]" : $"{this.Address}")} with {this.Buffer.Length} bytes of data:");
            this.Data.ForEach(pingReply => builder.AppendLine($"{pingReply}"));
            builder.AppendLine();
            builder.AppendLine($"Ping statistics for {this.Address}:");
            builder.AppendLine($"{new string(' ', 4)}Packets: Sent = {this.PacketsSent}, Received = {this.PacketsReceived}, Lost = {this.PacketsLost} ({this.PacketLoss:p0} loss),");
            builder.AppendLine($"Approximate round trip times in milli-seconds:");
            builder.AppendLine($"{new string(' ', 4)}Minimum = {this.MinRoundTripTime}ms, Maximum = {this.MaxRoundTripTime}ms, Average = {this.AveRoundTripTime:f0}ms");

            return builder.ToString();
        }
    }
}
