﻿using System.Collections.Generic;
using System.Linq;

namespace NetworkInformationModule.Models
{
    public class NetworkInterfaces
    {
        public IEnumerable<NetworkInterface> Data { get; set; }
        public long TotalBytesReceived => this.Data.Sum(networkInterface => networkInterface.IPInterfaceStatistics.BytesReceived);
        public long TotalBitsReceived => this.TotalBytesReceived * 8;
        public long TotalBytesSent => this.Data.Sum(networkInterface => networkInterface.IPInterfaceStatistics.BytesSent);
        public long TotalBitsSent => this.TotalBytesSent * 8;
        public long Speed => this.Data.Max(networkInterface => networkInterface.Speed);
        public double SpeedInKilobits => this.Speed / 1000D;
        public double SpeedInMegabits => this.SpeedInKilobits / 1000D;
    }
}
