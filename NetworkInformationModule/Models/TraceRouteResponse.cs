﻿using NetworkInformationModule.Extensions;
using System.Linq;
using System.Net.NetworkInformation;

namespace NetworkInformationModule.Models
{
    public class TraceRouteResponse
    {
        public PingReplies PingReplies { get; set; }
        public int HopId { get; set; }
        public string HostNameAndOrAddress => !this.PingReplies.HostName.IsNullOrWhiteSpace() ? $"{this.PingReplies.HostName} [{this.PingReplies.Address}]" : $"{this.PingReplies.Address}";

        public override string ToString()
        {
            var roundTripTimes = $"{this.PingReplies.Data.Select(pingReply => pingReply.Status != IPStatus.TimedOut ? $"{pingReply.RoundtripTime,6} ms" : $"{"*",9}").StringJoin(string.Empty)}";
            var hostNameAndOrAddress = this.PingReplies.PacketsLost != this.PingReplies.PacketsSent ? this.HostNameAndOrAddress : "Request timed out.";

            return $"{this.HopId,3}{roundTripTimes}{new string(' ', 2)}{hostNameAndOrAddress}";
        }
    }
}
