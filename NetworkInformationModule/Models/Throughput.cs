﻿using System.Text;

namespace NetworkInformationModule.Models
{
    public class Throughput
    {
        public double MaxTheoreticalThroughput { get; set; }
        public double MaxTheoreticalThroughputInKilobits => this.MaxTheoreticalThroughput / 1000D;
        public double MaxTheoreticalThroughputInMegabits => this.MaxTheoreticalThroughputInKilobits / 1000D;
        public double MaxActualThroughput { get; set; }
        public double MaxActualThroughputInKilobits => this.MaxActualThroughput / 1000D;
        public double MaxActualThroughputInMegabits => this.MaxActualThroughputInKilobits / 1000D;
        public double AveMaxActualThroughput { get; set; }
        public double AveMaxActualThroughputInKilobits => this.AveMaxActualThroughput / 1000D;
        public double AveMaxActualThroughputInMegabits => this.AveMaxActualThroughputInKilobits / 1000D;
        public double ThroughputPercentage => this.AveMaxActualThroughput / this.Speed;
        public long Speed { get; set; }
        public double SpeedInKilobits => this.Speed / 1000D;
        public double SpeedInMegabits => this.SpeedInKilobits / 1000D;

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Max Theoretical Throughput: {this.MaxTheoreticalThroughputInMegabits:n2} Mbps");
            builder.AppendLine($"Max Actual Throughput: {this.MaxActualThroughputInMegabits:n2} Mbps");
            builder.AppendLine($"Ave Nax Actual Throughput: {this.AveMaxActualThroughputInMegabits:n2} Mbps");
            builder.AppendLine($"Speed: {this.SpeedInMegabits:n2} Mbps");
            builder.AppendLine($"Throughput Percentage: {this.ThroughputPercentage:p2} Mbps");

            return builder.ToString();
        }
    }
}
