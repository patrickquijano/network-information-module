﻿using System.Net.Sockets;

namespace NetworkInformationModule.IFactories
{
    internal interface ITelnetTcpClientFactory
    {
        TcpClient Create(string hostNameOrAddress, int port);
    }
}