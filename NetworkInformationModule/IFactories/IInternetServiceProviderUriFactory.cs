﻿using System;

namespace NetworkInformationModule.IFactories
{
    internal interface IInternetServiceProviderUriFactory
    {
        Uri Create();
    }
}