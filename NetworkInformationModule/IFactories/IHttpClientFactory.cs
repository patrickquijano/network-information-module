﻿using System.Net.Http;

namespace NetworkInformationModule.IFactories
{
    internal interface IHttpClientFactory
    {
        HttpClient Create();
    }
}