﻿namespace NetworkInformationModule.IFactories
{
    internal interface IBufferFactory
    {
        byte[] Create();
        byte[] Create(int bufferSize);
    }
}