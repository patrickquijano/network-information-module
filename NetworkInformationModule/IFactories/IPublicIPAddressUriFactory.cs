﻿using System;

namespace NetworkInformationModule.IFactories
{
    internal interface IPublicIPAddressUriFactory
    {
        Uri Create();
    }
}