﻿using NetworkInformationModule.IFactories;
using NetworkInformationModule.IServices;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class GetInternetServiceProviderService : IGetInternetServiceProviderService
    {
        private readonly IHttpClientFactory _HttpClientFactory;
        private readonly IInternetServiceProviderUriFactory _InternetServiceProviderUriFactory;

        public GetInternetServiceProviderService(IHttpClientFactory httpClientFactory,
            IInternetServiceProviderUriFactory internetServiceProviderUriFactory)
        {
            this._HttpClientFactory = httpClientFactory;
            this._InternetServiceProviderUriFactory = internetServiceProviderUriFactory;
        }

        public string Get()
        {
            var uri = this._InternetServiceProviderUriFactory.Create();
            using (var httpClient = this._HttpClientFactory.Create())
            {
                try
                {
                    return httpClient.GetStringAsync(uri).Result;
                }
                catch
                {
                    return null;
                }
            }
        }

        public async Task<string> GetAsync()
        {
            var uri = this._InternetServiceProviderUriFactory.Create();
            using (var httpClient = this._HttpClientFactory.Create())
            {
                try
                {
                    return await httpClient.GetStringAsync(uri);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
