﻿using NetworkInformationModule.Extensions;
using NetworkInformationModule.IFactories;
using NetworkInformationModule.IServices;
using NetworkInformationModule.Models;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class GetPingRepliesService : IGetPingRepliesService
    {
        private readonly int _DefaultBufferSize;
        private readonly string _DefaultHostName;
        private readonly int _DefaultRoundTrips;
        private readonly int _DefaultTimeOut;
        private readonly IBufferFactory _BufferFactory;

        public GetPingRepliesService(int defaultBufferSize,
            string defaultHostName,
            int defaultRoundTrips,
            int defaultTimeOut,
            IBufferFactory bufferFactory)
        {
            this._DefaultBufferSize = defaultBufferSize;
            this._DefaultHostName = defaultHostName;
            this._DefaultRoundTrips = defaultRoundTrips;
            this._DefaultTimeOut = defaultTimeOut;
            this._BufferFactory = bufferFactory;
        }

        public PingReplies Get()
        {
            return this.Get(this._DefaultHostName);
        }

        public PingReplies Get(int roundTrips)
        {
            return this.Get(this._DefaultHostName, roundTrips);
        }

        public PingReplies Get(string hostNameOrAddress)
        {
            return this.Get(hostNameOrAddress, this._DefaultRoundTrips);
        }

        public PingReplies Get(string hostNameOrAddress, int roundTrips)
        {
            return this.Get(hostNameOrAddress, this._DefaultTimeOut, this._DefaultBufferSize, roundTrips);
        }

        public PingReplies Get(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips)
        {
            if (hostNameOrAddress.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is either a whitespace or a null.");
            }
            if (!hostNameOrAddress.IsValidHostNameOrIP())
            {
                throw new ArgumentException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is neither a valid host name nor valid IP address.");
            }
            if (timeOut < 1)
            {
                throw new ArgumentException($"The argument `{nameof(timeOut)}` of type {timeOut.GetType()} should be greater than 0.");
            }
            if (bufferSize < 1)
            {
                throw new ArgumentException($"The argument `{nameof(bufferSize)}` of type {bufferSize.GetType()} should be greater than 0.");
            }
            if (roundTrips < 1)
            {
                throw new ArgumentException($"The argument `{nameof(roundTrips)}` of type {roundTrips.GetType()} should be greater than 0.");
            }
            var hostName = hostNameOrAddress.IsValidHostName() ? hostNameOrAddress : hostNameOrAddress.GetHostName();
            var buffer = this._BufferFactory.Create(bufferSize);
            var pingReplies = new List<Models.PingReply>();
            for (var i = 0; i < roundTrips; i++)
            {
                using (var ping = new Ping())
                {
                    var pingReplyOriginal = ping.Send(hostNameOrAddress, timeOut, buffer);
                    var pingReply = new Models.PingReply
                    {
                        Address = pingReplyOriginal.Address,
                        Buffer = pingReplyOriginal.Buffer,
                        HostName = hostName,
                        Options = pingReplyOriginal.Options,
                        RoundtripTime = pingReplyOriginal.RoundtripTime,
                        Status = pingReplyOriginal.Status,
                    };
                    pingReplies.Add(pingReply);
                }
            }

            return new PingReplies { Data = pingReplies };
        }

        public Task<PingReplies> GetAsync()
        {
            return this.GetAsync(this._DefaultHostName);
        }

        public Task<PingReplies> GetAsync(int roundTripsCount)
        {
            return this.GetAsync(this._DefaultHostName, roundTripsCount);
        }

        public Task<PingReplies> GetAsync(string hostNameOrAddress)
        {
            return this.GetAsync(hostNameOrAddress, this._DefaultRoundTrips);
        }

        public Task<PingReplies> GetAsync(string hostNameOrAddress, int roundTripsCount)
        {
            return this.GetAsync(hostNameOrAddress, this._DefaultTimeOut, this._DefaultBufferSize, roundTripsCount);
        }

        public async Task<PingReplies> GetAsync(string hostNameOrAddress, int timeOut, int bufferSize, int roundTripsCount)
        {
            if (hostNameOrAddress.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is either a whitespace or a null.");
            }
            if (!hostNameOrAddress.IsValidHostNameOrIP())
            {
                throw new ArgumentException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is neither a valid host name nor valid IP address.");
            }
            if (timeOut < 1)
            {
                throw new ArgumentException($"The argument `{nameof(timeOut)}` of type {timeOut.GetType()} should be greater than 0.");
            }
            if (bufferSize < 1)
            {
                throw new ArgumentException($"The argument `{nameof(bufferSize)}` of type {bufferSize.GetType()} should be greater than 0.");
            }
            if (roundTripsCount < 1)
            {
                throw new ArgumentException($"The argument `{nameof(roundTripsCount)}` of type {roundTripsCount.GetType()} should be greater than 0.");
            }
            var hostName = hostNameOrAddress.IsValidHostName() ? hostNameOrAddress : hostNameOrAddress.GetHostName();
            var buffer = this._BufferFactory.Create(bufferSize);
            var tasks = new List<Task<Models.PingReply>>();
            for (var i = 0; i < roundTripsCount; i++)
            {
                var task = Task.Run(() =>
                {
                    using (var ping = new Ping())
                    {
                        var pingReplyOriginal = ping.Send(hostNameOrAddress, timeOut, buffer);

                        return new Models.PingReply
                        {
                            Address = pingReplyOriginal.Address,
                            Buffer = pingReplyOriginal.Buffer,
                            HostName = hostName,
                            Options = pingReplyOriginal.Options,
                            RoundtripTime = pingReplyOriginal.RoundtripTime,
                            Status = pingReplyOriginal.Status,
                        };
                    }
                });
                tasks.Add(task);
            }
            var pingReplies = await Task.WhenAll(tasks);

            return new PingReplies { Data = pingReplies };
        }
    }
}
