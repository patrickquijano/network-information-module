﻿using NetworkInformationModule.Extensions;
using NetworkInformationModule.IFactories;
using NetworkInformationModule.IServices;
using System;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class CheckTelnetService : ICheckTelnetService
    {
        private readonly string _DefaultHostName;
        private readonly ITelnetTcpClientFactory _TelnetTcpClientFactory;

        public CheckTelnetService(string defaultHostName,
            ITelnetTcpClientFactory telnetTcpClientFactory)
        {
            this._DefaultHostName = defaultHostName;
            this._TelnetTcpClientFactory = telnetTcpClientFactory;
        }

        public bool Check(int port)
        {
            return this.Check(port, null);
        }

        public bool Check(int port, Action<string> messageCallback)
        {
            return this.Check(this._DefaultHostName, port, messageCallback);
        }

        public bool Check(string hostNameOrAddress, int port)
        {
            return this.Check(hostNameOrAddress, port, null);
        }

        public bool Check(string hostNameOrAddress, int port, Action<string> messageCallback)
        {
            if (hostNameOrAddress.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is either a whitespace or a null.");
            }
            if (!hostNameOrAddress.IsValidHostNameOrIP())
            {
                throw new ArgumentException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is neither a valid host name nor valid IP address.");
            }
            if (port < 1 || port > 65535)
            {
                throw new ArgumentException($"The argument `{nameof(port)}` of type {port.GetType()} should be between 1 to 65535.");
            }
            try
            {
                using (var tcpClient = this._TelnetTcpClientFactory.Create(hostNameOrAddress, 443))
                {
                    messageCallback?.Invoke($"Successfully connected to {(hostNameOrAddress.IsValidHostName() ? "Host Name" : "IP Address")} {hostNameOrAddress} on port {port}.");

                    return true;
                }
            }
            catch
            {
                messageCallback?.Invoke($"Could not connect to {(hostNameOrAddress.IsValidHostName() ? "Host Name" : "IP Address")} {hostNameOrAddress} on port {port}.");

                return false;
            }
        }

        public Task<bool> CheckAsync(int port)
        {
            return this.CheckAsync(port, null);
        }

        public Task<bool> CheckAsync(int port, Action<string> messageCallback)
        {
            return this.CheckAsync(this._DefaultHostName, port, messageCallback);
        }

        public Task<bool> CheckAsync(string hostNameOrAddress, int port)
        {
            return this.CheckAsync(hostNameOrAddress, port, null);
        }

        public Task<bool> CheckAsync(string hostNameOrAddress, int port, Action<string> messageCallback)
        {
            return Task.Run(() => this.Check(hostNameOrAddress, port, messageCallback));
        }
    }
}
