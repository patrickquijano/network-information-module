﻿using NetworkInformationModule.Extensions;
using NetworkInformationModule.IServices;
using NetworkInformationModule.Models;
using NetworkInformationModule.States;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class GetThroughputService : IGetThroughputService
    {
        private readonly NetworkPerformanceState _NetworkPerformanceState;

        public GetThroughputService(NetworkPerformanceState networkPerformanceState)
        {
            this._NetworkPerformanceState = networkPerformanceState;
        }

        public Throughput Get()
        {
            var networkPerformances = this._NetworkPerformanceState.Get();
            var networkPerformanceDeltas = networkPerformances.SelectWithPrevious((previous, current) => new NetworkPerformanceDelta
            {
                BytesReceivedDelta = Math.Abs(current.BytesReceived - previous.BytesReceived),
                BytesSentDelta = Math.Abs(current.BytesSent - previous.BytesSent),
                Speed = Math.Max(previous.Speed, current.Speed),
            });

            return new Throughput
            {
                AveMaxActualThroughput = networkPerformanceDeltas.Average(networkPerformance => networkPerformance.MaxActualThroughput),
                MaxActualThroughput = networkPerformanceDeltas.Max(networkPerformance => networkPerformance.MaxActualThroughput),
                MaxTheoreticalThroughput = networkPerformanceDeltas.Max(networkPerformance => networkPerformance.MaxTheoreticalThroughput),
                Speed = networkPerformanceDeltas.Max(networkPerformance => networkPerformance.Speed),
            };
        }

        public Task<Throughput> GetAsync()
        {
            return Task.Run(() => this.Get());
        }
    }
}
