﻿using NetworkInformationModule.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class GetNetworkInterfacesService : IGetNetworkInterfacesService
    {
        public Models.NetworkInterfaces Get()
        {
            return this.Get(null);
        }

        public Models.NetworkInterfaces Get(bool isUpAndNonLoopbackAndNonVirtual)
        {
            var conditions = new List<Func<Models.NetworkInterface, bool>>();
            if (isUpAndNonLoopbackAndNonVirtual)
            {
                conditions.Add(networkInterface => networkInterface.OperationalStatus == OperationalStatus.Up);
                conditions.Add(networkInterface => networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback);
                foreach (var excludedDescription in new string[] { "Cisco", "Fortinet", "Hyper-V", "VirtualBox" })
                {
                    conditions.Add(networkInterface => !networkInterface.Description.ToLower().Contains(excludedDescription.ToLower()));
                }
            }

            return this.Get(conditions);
        }

        public Models.NetworkInterfaces Get(IEnumerable<Func<Models.NetworkInterface, bool>> conditions)
        {
            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces()
                .Select(originalNetworkInterface => new Models.NetworkInterface
                {
                    OperationalStatus = originalNetworkInterface.OperationalStatus,
                    NetworkInterfaceType = originalNetworkInterface.NetworkInterfaceType,
                    Name = originalNetworkInterface.Name,
                    IsReceiveOnly = originalNetworkInterface.IsReceiveOnly,
                    Id = originalNetworkInterface.Id,
                    Description = originalNetworkInterface.Description,
                    Speed = originalNetworkInterface.Speed,
                    SupportsMulticast = originalNetworkInterface.SupportsMulticast,
                    IPInterfaceProperties = originalNetworkInterface.GetIPProperties(),
                    IPv4InterfaceProperties = originalNetworkInterface.GetIPProperties().GetIPv4Properties(),
                    IPv6InterfaceProperties = originalNetworkInterface.GetIPProperties().GetIPv6Properties(),
                    IPInterfaceStatistics = originalNetworkInterface.GetIPStatistics(),
                    IPv4InterfaceStatistics = originalNetworkInterface.GetIPv4Statistics(),
                    PhysicalAddress = originalNetworkInterface.GetPhysicalAddress(),
                    SupportsIPv4 = originalNetworkInterface.Supports(NetworkInterfaceComponent.IPv4),
                    SupportsIPv6 = originalNetworkInterface.Supports(NetworkInterfaceComponent.IPv6),
                });
            if (conditions != null && conditions.Count() > 0)
            {
                foreach (var condition in conditions)
                {
                    networkInterfaces = networkInterfaces.Where(condition);
                }
            }

            return new Models.NetworkInterfaces { Data = networkInterfaces };
        }

        public Task<Models.NetworkInterfaces> GetAsync()
        {
            return this.GetAsync(null);
        }

        public Task<Models.NetworkInterfaces> GetAsync(bool isUpAndNonLoopbackAndNonVirtual)
        {
            var conditions = new List<Func<Models.NetworkInterface, bool>>();
            if (isUpAndNonLoopbackAndNonVirtual)
            {
                conditions.Add(networkInterface => networkInterface.OperationalStatus == OperationalStatus.Up);
                conditions.Add(networkInterface => networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback);
                foreach (var excludedDescription in new string[] { "Cisco", "Fortinet", "Hyper-V", "VirtualBox" })
                {
                    conditions.Add(networkInterface => !networkInterface.Description.ToLower().Contains(excludedDescription.ToLower()));
                }
            }

            return this.GetAsync(conditions);
        }

        public Task<Models.NetworkInterfaces> GetAsync(IEnumerable<Func<Models.NetworkInterface, bool>> conditions)
        {
            return Task.Run(() => this.Get(conditions));
        }
    }
}
