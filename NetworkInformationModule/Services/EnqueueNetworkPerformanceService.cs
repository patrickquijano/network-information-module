﻿using NetworkInformationModule.IServices;
using NetworkInformationModule.Models;
using NetworkInformationModule.States;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class EnqueueNetworkPerformanceService : IEnqueueNetworkPerformanceService
    {
        private readonly NetworkPerformanceState _NetworkPerformaceState;
        private readonly IGetNetworkInterfacesService _GetNetworkInterfacesService;

        public EnqueueNetworkPerformanceService(NetworkPerformanceState networkPerformaceState,
            IGetNetworkInterfacesService getNetworkInterfacesService)
        {
            this._NetworkPerformaceState = networkPerformaceState;
            this._GetNetworkInterfacesService = getNetworkInterfacesService;
        }

        public void Enqueue()
        {
            var networkInterfaces = this._GetNetworkInterfacesService.Get(true);
            var networkPerformance = new NetworkPerformance
            {
                BytesReceived = networkInterfaces.TotalBytesReceived,
                BytesSent = networkInterfaces.TotalBytesSent,
                Speed = networkInterfaces.Speed,
            };
            this._NetworkPerformaceState.Enqueue(networkPerformance);
        }

        public Task EnqueueAsync()
        {
            return Task.Run(() => this.Enqueue());
        }
    }
}
