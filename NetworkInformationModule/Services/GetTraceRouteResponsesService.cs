﻿using NetworkInformationModule.Extensions;
using NetworkInformationModule.IFactories;
using NetworkInformationModule.IServices;
using NetworkInformationModule.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class GetTraceRouteResponsesService : IGetTraceRouteResponsesService
    {
        private readonly int _DefaultBufferSize;
        private readonly string _DefaultHostName;
        private readonly int _DefaultMaxHops;
        private readonly int _DefaultRoundTrips;
        private readonly int _DefaultTimeOut;
        private readonly IBufferFactory _BufferFactory;

        public GetTraceRouteResponsesService(int defaultBufferSize,
            string defaultHostName,
            int defaultMaxHops,
            int defaultRoundTrips,
            int defaultTimeOut,
            IBufferFactory bufferFactory)
        {
            this._DefaultBufferSize = defaultBufferSize;
            this._DefaultHostName = defaultHostName;
            this._DefaultMaxHops = defaultMaxHops;
            this._DefaultRoundTrips = defaultRoundTrips;
            this._DefaultTimeOut = defaultTimeOut;
            this._BufferFactory = bufferFactory;
        }

        public TraceRouteResponses Get()
        {
            return this.Get(this._DefaultHostName);
        }

        public TraceRouteResponses Get(int roundTrips)
        {
            return this.Get(this._DefaultHostName, roundTrips);
        }

        public TraceRouteResponses Get(string hostNameOrAddress)
        {
            return this.Get(hostNameOrAddress, this._DefaultRoundTrips);
        }

        public TraceRouteResponses Get(string hostNameOrAddress, int roundTrips)
        {
            return this.Get(hostNameOrAddress, roundTrips, this._DefaultMaxHops);
        }

        public TraceRouteResponses Get(string hostNameOrAddress, int roundTrips, int maxHops)
        {
            return this.Get(hostNameOrAddress, this._DefaultTimeOut, this._DefaultBufferSize, roundTrips, maxHops);
        }

        public TraceRouteResponses Get(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips, int maxHops)
        {
            if (hostNameOrAddress.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is either a whitespace or a null.");
            }
            if (!hostNameOrAddress.IsValidHostNameOrIP())
            {
                throw new ArgumentException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is neither a valid host name nor valid IP address.");
            }
            if (timeOut < 1)
            {
                throw new ArgumentException($"The argument `{nameof(timeOut)}` of type {timeOut.GetType()} should be greater than 0.");
            }
            if (bufferSize < 1)
            {
                throw new ArgumentException($"The argument `{nameof(bufferSize)}` of type {bufferSize.GetType()} should be greater than 0.");
            }
            if (roundTrips < 1)
            {
                throw new ArgumentException($"The argument `{nameof(roundTrips)}` of type {roundTrips.GetType()} should be greater than 0.");
            }
            if (maxHops < 1)
            {
                throw new ArgumentException($"The argument `{nameof(maxHops)}` of type {maxHops.GetType()} should be greater than 0.");
            }
            var buffer = this._BufferFactory.Create(bufferSize);
            var traceRouteResponses = new List<TraceRouteResponse>();
            for (var i = 0; i < maxHops; i++)
            {
                var pingReplies = new List<Models.PingReply>();
                for (var j = 0; j < roundTrips; j++)
                {
                    using (var ping = new Ping())
                    {
                        var stopwatch = Stopwatch.StartNew();
                        var pingReplyOriginal = ping.Send(hostNameOrAddress, timeOut, buffer, new PingOptions(i + 1, false));
                        stopwatch.Stop();
                        var pingReply = new Models.PingReply
                        {
                            Address = pingReplyOriginal.Address,
                            Buffer = pingReplyOriginal.Buffer,
                            HostName = !pingReplyOriginal.Address.IsPrivate() ? pingReplyOriginal.Address.GetHostName() : null,
                            Options = pingReplyOriginal.Options,
                            RoundtripTime = stopwatch.ElapsedMilliseconds,
                            Status = pingReplyOriginal.Status,
                        };
                        pingReplies.Add(pingReply);
                    }
                }
                var traceRouteResponse = new TraceRouteResponse
                {
                    HopId = i + 1,
                    PingReplies = new PingReplies { Data = pingReplies },
                };
                traceRouteResponses.Add(traceRouteResponse);
                if (traceRouteResponse.PingReplies.Data.Any(pingReply => pingReply.Status != IPStatus.TtlExpired && pingReply.Status != IPStatus.TimedOut))
                {
                    break;
                }
            }
            var hostName = string.Empty;
            var address = (IPAddress)null;
            if (hostNameOrAddress.IsValidIP())
            {
                address = IPAddress.Parse(hostNameOrAddress);
                hostName = address.GetHostName();
            }
            else
            {
                hostName = hostNameOrAddress;
                address = hostNameOrAddress.GetIPFromHostName();
            }

            return new TraceRouteResponses
            {
                Address = address,
                Data = traceRouteResponses,
                HostName = hostName,
                MaxHops = maxHops,
            };
        }

        public Task<TraceRouteResponses> GetAsync()
        {
            return this.GetAsync(this._DefaultHostName);
        }

        public Task<TraceRouteResponses> GetAsync(int roundTrips)
        {
            return this.GetAsync(this._DefaultHostName, roundTrips);
        }

        public Task<TraceRouteResponses> GetAsync(string hostNameOrAddress)
        {
            return this.GetAsync(hostNameOrAddress, this._DefaultRoundTrips);
        }

        public Task<TraceRouteResponses> GetAsync(string hostNameOrAddress, int roundTrips)
        {
            return this.GetAsync(hostNameOrAddress, roundTrips, this._DefaultMaxHops);
        }

        public Task<TraceRouteResponses> GetAsync(string hostNameOrAddress, int roundTrips, int maxHops)
        {
            return this.GetAsync(hostNameOrAddress, this._DefaultTimeOut, this._DefaultBufferSize, roundTrips, maxHops);
        }

        public async Task<TraceRouteResponses> GetAsync(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips, int maxHops)
        {
            if (hostNameOrAddress.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is either a whitespace or a null.");
            }
            if (!hostNameOrAddress.IsValidHostNameOrIP())
            {
                throw new ArgumentException($"The argument `{nameof(hostNameOrAddress)}` of type {hostNameOrAddress.GetType()} is neither a valid host name nor valid IP address.");
            }
            if (timeOut < 1)
            {
                throw new ArgumentException($"The argument `{nameof(timeOut)}` of type {timeOut.GetType()} should be greater than 0.");
            }
            if (bufferSize < 1)
            {
                throw new ArgumentException($"The argument `{nameof(bufferSize)}` of type {bufferSize.GetType()} should be greater than 0.");
            }
            if (roundTrips < 1)
            {
                throw new ArgumentException($"The argument `{nameof(roundTrips)}` of type {roundTrips.GetType()} should be greater than 0.");
            }
            if (maxHops < 1)
            {
                throw new ArgumentException($"The argument `{nameof(maxHops)}` of type {maxHops.GetType()} should be greater than 0.");
            }
            var buffer = this._BufferFactory.Create(bufferSize);
            var traceRouteResponseTasks = new List<Task<TraceRouteResponse>>();
            for (var i = 0; i < maxHops; i++)
            {
                var hopId = i + 1;
                var pingOptions = new PingOptions(hopId, false);
                var traceRouteResponseTask = Task.Run(async () =>
                {
                    var pingReplyTasks = new List<Task<Models.PingReply>>();
                    for (var j = 0; j < roundTrips + 1; j++)
                    {
                        var pingReplyTask = Task.Factory.StartNew(() =>
                        {
                            using (var ping = new Ping())
                            {
                                var stopwatch = Stopwatch.StartNew();
                                var pingReplyOriginal = ping.Send(hostNameOrAddress, timeOut, buffer, pingOptions);
                                stopwatch.Stop();

                                return new Models.PingReply
                                {
                                    Address = pingReplyOriginal.Address,
                                    Buffer = pingReplyOriginal.Buffer,
                                    HostName = !pingReplyOriginal.Address.IsPrivate() ? pingReplyOriginal.Address.GetHostName() : null,
                                    Options = pingReplyOriginal.Options,
                                    RoundtripTime = stopwatch.ElapsedMilliseconds,
                                    Status = pingReplyOriginal.Status,
                                };
                            }
                        }, TaskCreationOptions.AttachedToParent);
                        pingReplyTasks.Add(pingReplyTask);
                    }
                    var pingReplies = await Task.WhenAll(pingReplyTasks);
                    pingReplies = pingReplies.Take(roundTrips).ToArray();

                    return new TraceRouteResponse
                    {
                        HopId = hopId,
                        PingReplies = new PingReplies { Data = pingReplies },
                    };
                });
                traceRouteResponseTasks.Add(traceRouteResponseTask);
            }
            var traceRouteResponses = (await Task.WhenAll(traceRouteResponseTasks)).ToList();
            for (var i = traceRouteResponses.Count; i > 0; i--)
            {
                var traceRouteResponse = traceRouteResponses.Skip(i - 1).FirstOrDefault();
                if (traceRouteResponse == null)
                {
                    continue;
                }
                if (traceRouteResponse.PingReplies.Data.Any(pingReply => pingReply.Status != IPStatus.TtlExpired && pingReply.Status != IPStatus.TimedOut))
                {
                    traceRouteResponses.RemoveAt(i - 1);
                }
                if (traceRouteResponses.Count(traceRouteResponse1 => traceRouteResponse1.PingReplies.Data.Any(pingReply => pingReply.Status != IPStatus.TtlExpired && pingReply.Status != IPStatus.TimedOut)) < 2)
                {
                    break;
                }
            }
            var hostName = string.Empty;
            var address = (IPAddress)null;
            if (hostNameOrAddress.IsValidIP())
            {
                address = IPAddress.Parse(hostNameOrAddress);
                hostName = await address.GetHostNameAsync();
            }
            else
            {
                hostName = hostNameOrAddress;
                address = hostNameOrAddress.GetIPFromHostName();
            }

            return new TraceRouteResponses
            {
                Address = address,
                Data = traceRouteResponses,
                HostName = hostName,
                MaxHops = maxHops,
            };
        }
    }
}
