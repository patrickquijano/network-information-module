﻿using NetworkInformationModule.IFactories;
using NetworkInformationModule.IServices;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetworkInformationModule.Services
{
    internal class GetPublicIPAddressService : IGetPublicIPAddressService
    {
        private readonly IHttpClientFactory _HttpClientFactory;
        private readonly IPublicIPAddressUriFactory _PublicIPAddressUriFactory;

        public GetPublicIPAddressService(IHttpClientFactory httpClientFactory,
            IPublicIPAddressUriFactory publicIPAddressUriFactory)
        {
            this._HttpClientFactory = httpClientFactory;
            this._PublicIPAddressUriFactory = publicIPAddressUriFactory;
        }

        public IPAddress Get()
        {
            var uri = this._PublicIPAddressUriFactory.Create();
            using (var httpClient = this._HttpClientFactory.Create())
            {
                try
                {
                    var stringResponse = httpClient.GetStringAsync(uri).Result;
                    var address = Regex.Match(stringResponse, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");

                    return IPAddress.Parse(address.Value);
                }
                catch
                {
                    return null;
                }
            }
        }

        public async Task<IPAddress> GetAsync()
        {
            var uri = this._PublicIPAddressUriFactory.Create();
            using (var httpClient = this._HttpClientFactory.Create())
            {
                try
                {
                    var stringResponse = await httpClient.GetStringAsync(uri);
                    var address = Regex.Match(stringResponse, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");

                    return IPAddress.Parse(address.Value);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
