﻿using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IGetInternetServiceProviderService
    {
        string Get();
        Task<string> GetAsync();
    }
}