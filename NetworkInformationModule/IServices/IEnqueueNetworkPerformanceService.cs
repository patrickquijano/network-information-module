﻿using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IEnqueueNetworkPerformanceService
    {
        void Enqueue();
        Task EnqueueAsync();
    }
}