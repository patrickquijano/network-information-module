﻿using NetworkInformationModule.Models;
using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IGetPingRepliesService
    {
        PingReplies Get();
        PingReplies Get(int roundTrips);
        PingReplies Get(string hostNameOrAddress);
        PingReplies Get(string hostNameOrAddress, int roundTrips);
        PingReplies Get(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips);
        Task<PingReplies> GetAsync();
        Task<PingReplies> GetAsync(int roundTrips);
        Task<PingReplies> GetAsync(string hostNameOrAddress);
        Task<PingReplies> GetAsync(string hostNameOrAddress, int roundTrips);
        Task<PingReplies> GetAsync(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips);
    }
}