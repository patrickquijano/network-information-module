﻿using System.Net;
using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IGetPublicIPAddressService
    {
        IPAddress Get();
        Task<IPAddress> GetAsync();
    }
}