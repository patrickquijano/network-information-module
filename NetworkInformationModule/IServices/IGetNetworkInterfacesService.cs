﻿using NetworkInformationModule.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IGetNetworkInterfacesService
    {
        NetworkInterfaces Get();
        NetworkInterfaces Get(bool isUpAndNonLoopbackAndNonVirtual);
        NetworkInterfaces Get(IEnumerable<Func<NetworkInterface, bool>> conditions);
        Task<NetworkInterfaces> GetAsync();
        Task<NetworkInterfaces> GetAsync(bool isUpAndNonLoopbackAndNonVirtual);
        Task<NetworkInterfaces> GetAsync(IEnumerable<Func<NetworkInterface, bool>> conditions);
    }
}