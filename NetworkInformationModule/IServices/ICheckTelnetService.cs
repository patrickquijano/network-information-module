﻿using System;
using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface ICheckTelnetService
    {
        bool Check(int port);
        bool Check(int port, Action<string> messageCallback);
        bool Check(string hostNameOrAddress, int port);
        bool Check(string hostNameOrAddress, int port, Action<string> messageCallback);
        Task<bool> CheckAsync(int port);
        Task<bool> CheckAsync(int port, Action<string> messageCallback);
        Task<bool> CheckAsync(string hostNameOrAddress, int port);
        Task<bool> CheckAsync(string hostNameOrAddress, int port, Action<string> messageCallback);
    }
}