﻿using NetworkInformationModule.Models;
using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IGetThroughputService
    {
        Throughput Get();
        Task<Throughput> GetAsync();
    }
}