﻿using NetworkInformationModule.Models;
using System.Threading.Tasks;

namespace NetworkInformationModule.IServices
{
    public interface IGetTraceRouteResponsesService
    {
        TraceRouteResponses Get();
        TraceRouteResponses Get(int roundTrips);
        TraceRouteResponses Get(string hostNameOrAddress);
        TraceRouteResponses Get(string hostNameOrAddress, int roundTrips);
        TraceRouteResponses Get(string hostNameOrAddress, int roundTrips, int maxHops);
        TraceRouteResponses Get(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips, int maxHops);
        Task<TraceRouteResponses> GetAsync();
        Task<TraceRouteResponses> GetAsync(int roundTrips);
        Task<TraceRouteResponses> GetAsync(string hostNameOrAddress);
        Task<TraceRouteResponses> GetAsync(string hostNameOrAddress, int roundTrips);
        Task<TraceRouteResponses> GetAsync(string hostNameOrAddress, int roundTrips, int maxHops);
        Task<TraceRouteResponses> GetAsync(string hostNameOrAddress, int timeOut, int bufferSize, int roundTrips, int maxHops);
    }
}