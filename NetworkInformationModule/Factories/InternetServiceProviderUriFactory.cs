﻿using NetworkInformationModule.IFactories;
using System;

namespace NetworkInformationModule.Factories
{
    internal class InternetServiceProviderUriFactory : IInternetServiceProviderUriFactory
    {
        private readonly string _UrlSource;

        public InternetServiceProviderUriFactory(string urlSource)
        {
            this._UrlSource = urlSource;
        }

        public Uri Create()
        {
            return new Uri(this._UrlSource);
        }
    }
}
