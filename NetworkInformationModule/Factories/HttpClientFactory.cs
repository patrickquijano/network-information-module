﻿using NetworkInformationModule.IFactories;
using System.Net.Http;

namespace NetworkInformationModule.Factories
{
    internal class HttpClientFactory : IHttpClientFactory
    {
        public HttpClient Create()
        {
            return new HttpClient();
        }
    }
}
