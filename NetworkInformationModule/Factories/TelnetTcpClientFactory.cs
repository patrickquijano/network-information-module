﻿using NetworkInformationModule.IFactories;
using System.Net.Sockets;

namespace NetworkInformationModule.Factories
{
    internal class TelnetTcpClientFactory : ITelnetTcpClientFactory
    {
        public TcpClient Create(string hostNameOrAddress, int port)
        {
            return new TcpClient(hostNameOrAddress, port);
        }
    }
}
