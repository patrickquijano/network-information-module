﻿using NetworkInformationModule.IFactories;
using System.Text;

namespace NetworkInformationModule.Factories
{
    internal class BufferFactory : IBufferFactory
    {
        public byte[] Create()
        {
            return this.Create(32);
        }

        public byte[] Create(int bufferSize)
        {
            var data = new string('a', bufferSize);
            var buffer = Encoding.ASCII.GetBytes(data);

            return buffer;
        }
    }
}
