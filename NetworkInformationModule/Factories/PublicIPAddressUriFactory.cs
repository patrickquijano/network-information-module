﻿using NetworkInformationModule.IFactories;
using System;

namespace NetworkInformationModule.Factories
{
    internal class PublicIPAddressUriFactory : IPublicIPAddressUriFactory
    {
        private readonly string _UrlSource;

        public PublicIPAddressUriFactory(string urlSource)
        {
            this._UrlSource = urlSource;
        }

        public Uri Create()
        {
            return new Uri(this._UrlSource);
        }
    }
}
