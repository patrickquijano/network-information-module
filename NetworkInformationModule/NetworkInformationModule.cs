﻿using Autofac;
using NetworkInformationModule.Factories;
using NetworkInformationModule.IFactories;
using NetworkInformationModule.IServices;
using NetworkInformationModule.Services;
using NetworkInformationModule.States;

namespace NetworkInformationModule
{
    public class NetworkInformationModule : Module
    {
        public int DefaultPingBufferSize { get; set; }
        public string DefaultPingHostName { get; set; }
        public int DefaultPingTimeout { get; set; }
        public int DefaultPingRoundTrips { get; set; }
        public string DefaultTelnetHostName { get; set; }
        public int DefaultTraceRouteBufferSize { get; set; }
        public string DefaultTraceRouteHostName { get; set; }
        public int DefaultTraceRouteMaxHops { get; set; }
        public int DefaultTraceRouteTimeout { get; set; }
        public int DefaultTraceRouteRoundTrips { get; set; }
        public string InternetServiceProviderUrlSource { get; set; }
        public string PublicIPAddressUrlSource { get; set; }

        public NetworkInformationModule()
        {
            this.DefaultPingBufferSize = 32;
            this.DefaultPingHostName = "dns.google";
            this.DefaultPingTimeout = 1000;
            this.DefaultPingRoundTrips = 4;
            this.DefaultTelnetHostName = "dns.google";
            this.DefaultTraceRouteBufferSize = 32;
            this.DefaultTraceRouteHostName = "dns.google";
            this.DefaultTraceRouteMaxHops = 30;
            this.DefaultTraceRouteTimeout = 1000;
            this.DefaultTraceRouteRoundTrips = 3;
            this.InternetServiceProviderUrlSource = "http://ip-api.com/line/?fields=isp";
            this.PublicIPAddressUrlSource = "https://checkip.amazonaws.com/";
        }

        protected override void Load(ContainerBuilder builder)
        {
            this.RegisterFactories(builder);
            this.RegisterServices(builder);
            this.RegisterStates(builder);
        }

        private void RegisterFactories(ContainerBuilder builder)
        {
            builder.RegisterType<BufferFactory>().As<IBufferFactory>().SingleInstance();
            builder.RegisterType<HttpClientFactory>().As<IHttpClientFactory>().SingleInstance();
            builder.RegisterType<InternetServiceProviderUriFactory>().As<IInternetServiceProviderUriFactory>().SingleInstance()
                .WithParameter("urlSource", this.InternetServiceProviderUrlSource);
            builder.RegisterType<PublicIPAddressUriFactory>().As<IPublicIPAddressUriFactory>().SingleInstance()
                .WithParameter("urlSource", this.PublicIPAddressUrlSource);
            builder.RegisterType<TelnetTcpClientFactory>().As<ITelnetTcpClientFactory>().SingleInstance();
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<CheckTelnetService>().As<ICheckTelnetService>().SingleInstance()
                .WithParameter("defaultHostName", this.DefaultTelnetHostName);
            builder.RegisterType<EnqueueNetworkPerformanceService>().As<IEnqueueNetworkPerformanceService>().SingleInstance();
            builder.RegisterType<GetInternetServiceProviderService>().As<IGetInternetServiceProviderService>().SingleInstance();
            builder.RegisterType<GetNetworkInterfacesService>().As<IGetNetworkInterfacesService>().SingleInstance();
            builder.RegisterType<GetPingRepliesService>().As<IGetPingRepliesService>().SingleInstance()
                .WithParameter("defaultBufferSize", this.DefaultPingBufferSize)
                .WithParameter("defaultHostName", this.DefaultPingHostName)
                .WithParameter("defaultRoundTrips", this.DefaultPingRoundTrips)
                .WithParameter("defaultTimeOut", this.DefaultPingTimeout);
            builder.RegisterType<GetPublicIPAddressService>().As<IGetPublicIPAddressService>().SingleInstance();
            builder.RegisterType<GetThroughputService>().As<IGetThroughputService>().SingleInstance();
            builder.RegisterType<GetTraceRouteResponsesService>().As<IGetTraceRouteResponsesService>().SingleInstance()
                .WithParameter("defaultBufferSize", this.DefaultTraceRouteBufferSize)
                .WithParameter("defaultHostName", this.DefaultTraceRouteHostName)
                .WithParameter("defaultMaxHops", this.DefaultTraceRouteMaxHops)
                .WithParameter("defaultRoundTrips", this.DefaultTraceRouteRoundTrips)
                .WithParameter("defaultTimeOut", this.DefaultTraceRouteTimeout);
        }

        private void RegisterStates(ContainerBuilder builder)
        {
            builder.RegisterType<NetworkPerformanceState>().SingleInstance();
        }
    }
}
