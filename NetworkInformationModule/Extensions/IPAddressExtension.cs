﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NetworkInformationModule.Extensions
{
    internal static class IPAddressExtension
    {
        public static string GetHostName(this IPAddress input)
        {
            try
            {
                return Dns.GetHostEntry(input).HostName;
            }
            catch
            {
                return null;
            }
        }

        public static async Task<string> GetHostNameAsync(this IPAddress input)
        {
            try
            {
                return (await Dns.GetHostEntryAsync(input)).HostName;
            }
            catch
            {
                return null;
            }
        }

        public static bool IsPrivate(this IPAddress input)
        {
            if (IPAddress.IsLoopback(input))
            {
                return true;
            }
            else if (input.ToString() == "::1")
            {
                return false;
            }
            byte[] bytes = input.GetAddressBytes();
            var byte1 = bytes.FirstOrDefault();
            var byte2 = bytes.Skip(1).FirstOrDefault();
            switch (byte1)
            {
                case 10:
                    return true;
                case 172:
                    return byte2 < 32 && byte2 >= 16;
                case 192:
                    return byte2 == 168;
                default:
                    return false;
            }
        }
    }
}
