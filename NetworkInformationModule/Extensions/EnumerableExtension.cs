﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NetworkInformationModule.Extensions
{
    internal static class EnumerableExtension
    {
        public static void ForEach<T>(this IEnumerable<T> input, Action<T> callback)
        {
            if (input == null || !input.Any())
            {
                return;
            }
            foreach (var item in input)
            {
                callback(item);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> input, Action<T, int> callback)
        {
            if (input == null || !input.Any())
            {
                return;
            }
            var i = 0;
            foreach (var item in input)
            {
                callback(item, i);
                i++;
            }
        }

        public static IEnumerable<TResult> SelectWithPrevious<TSource, TResult>(this IEnumerable<TSource> input, Func<TSource, TSource, TResult> projection)
        {
            using (var iterator = input.GetEnumerator())
            {
                if (!iterator.MoveNext())
                {
                    yield break;
                }
                var previous = iterator.Current;
                while (iterator.MoveNext())
                {
                    yield return projection(previous, iterator.Current);
                    previous = iterator.Current;
                }
            }
        }

        public static string StringJoin(this IEnumerable<string> input, string separator)
        {
            if (input == null || !input.Any())
            {
                return null;
            }

            return string.Join(separator, input);
        }
    }
}
