﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NetworkInformationModule.Extensions
{
    internal static class StringExtension
    {
        public static string GetHostName(this string input)
        {
            if (!input.IsValidUrlOrHostNameOrIP())
            {
                return null;
            }
            if (input.IsValidIP() || input.IsValidHostName())
            {
                return input;
            }

            return new Uri(input).Host;
        }

        public static IPAddress GetIPFromHostName(this string input)
        {
            if (!input.IsValidHostName())
            {
                return default;
            }
            var hostEntry = Dns.GetHostEntry(input);

            return hostEntry.AddressList.FirstOrDefault();
        }

        public static async Task<IPAddress> GetIPFromHostNameAsync(this string input)
        {
            if (!input.IsValidHostName())
            {
                return default;
            }
            var hostEntry = await Dns.GetHostEntryAsync(input);

            return hostEntry.AddressList.FirstOrDefault();
        }

        public static IPAddress GetIPFromUrl(this string input)
        {
            if (!input.IsValidUrl())
            {
                return default;
            }
            var uri = new Uri(input);

            return Dns.GetHostAddresses(uri.Host).FirstOrDefault();
        }

        public static async Task<IPAddress> GetIPFromUrlAsync(this string input)
        {
            if (!input.IsValidUrl())
            {
                return default;
            }
            var uri = new Uri(input);

            return (await Dns.GetHostAddressesAsync(uri.Host)).FirstOrDefault();
        }

        public static bool IsNullOrWhiteSpace(this string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }

        public static bool IsValidHostName(this string input)
        {
            return Uri.CheckHostName(input) == UriHostNameType.Dns;
        }

        public static bool IsValidHostNameOrIP(this string input)
        {
            return input.IsValidHostName() || input.IsValidIP();
        }

        public static bool IsValidIP(this string input)
        {
            return Uri.CheckHostName(input) == UriHostNameType.IPv4 || Uri.CheckHostName(input) == UriHostNameType.IPv6;
        }

        public static bool IsValidUrl(this string input)
        {
            return Uri.TryCreate(input, UriKind.Absolute, out var uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        public static bool IsValidUrlOrHostNameOrIP(this string input)
        {
            return input.IsValidUrl() || input.IsValidHostName() || input.IsValidIP();
        }
    }
}
