﻿using NetworkInformationModule.Models;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetworkInformationModule.States
{
    internal class NetworkPerformanceState
    {
        private readonly ConcurrentQueue<NetworkPerformance> _NetworkPerformances;

        public NetworkPerformanceState()
        {
            this._NetworkPerformances = new ConcurrentQueue<NetworkPerformance>();
        }

        public void Enqueue(NetworkPerformance networkPerformance)
        {
            this._NetworkPerformances.Enqueue(networkPerformance);
            lock (this._NetworkPerformances)
            {
                while (this._NetworkPerformances.Count > 60)
                {
                    this._NetworkPerformances.TryDequeue(out _);
                }
            }
        }

        public Task EnqueueAsync(NetworkPerformance networkPerformance)
        {
            return Task.Run(() => this.Enqueue(networkPerformance));
        }

        public IEnumerable<NetworkPerformance> Get()
        {
            return this._NetworkPerformances;
        }

        public Task<IEnumerable<NetworkPerformance>> GetAsync()
        {
            return Task.Run(() => this.Get());
        }
    }
}
