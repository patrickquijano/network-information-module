# Network Information Module

An Autofac module that gets the network information like ping and trace routes.

## Usage

Register the module in the Container.

```C#
var builder = new Autofac.ContainerBuilder();
builder.RegisterModule<NetworkInformationModule.NetworkInformationModule>();
```

Implement the services  `NetworkInformationModule.IServices.IGetPingRepliesService` and `NetworkInformationModule.IServices.IGetTraceRouteResponsesService`.

```C#
using NetworkInformationModule.IServices;
using System.Diagnostics;
// other imports

namespace SampleNamespace
{
	public class SampleClass
	{
		private readonly ICheckTelnetService _CheckTelnetService;
		private readonly IGetInternetServiceProviderService _GetInternetServiceProviderService;
		private readonly IGetPingRepliesService _GetPingRepliesService;
		private readonly IGetPublicIPAddressService _GetPublicIPAddressService;
		private readonly IGetTraceRouteResponsesService _GetTraceRouteResponsesService;

		public SampleClass(ICheckTelnetService checkTelnetService, IGetInternetServiceProviderService getInternetServiceProviderService, IGetPingRepliesService getPingRepliesService, IGetPublicIPAddressService getPublicIPAddressService, IGetTraceRouteResponsesService getTraceRouteResponsesService)
		{
			this._CheckTelnetService = checkTelnetService;
			this._GetInternetServiceProviderService = getInternetServiceProviderService;
			this._GetPingRepliesService = getPingRepliesService;
			this.IGetPublicIPAddressService = getPublicIPAddressService;
			this._GetTraceRouteResponsesService = getTraceRouteResponsesService;
			// other implementations
		}

		public async Task SampleMethod()
		{
			var telnetResult = await this._CheckTelnetService.CheckAsync();
			var internetServiceProvider = await this._GetInternetServiceProviderService.GetAsync();
			var pingReplies = await this._GetPingRepliesService.GetAsync();
			var publicIPAddress = await this._GetPublicIPAddressService.GetAsync();
			var traceRouteResponses = await this._GetTraceRouteResponsesService.GetAsync();
			Debug.WriteLine(telnetResult);
			Debug.WriteLine(internetServiceProvider);
			Debug.WriteLine(pingReplies);
			Debug.WriteLine(publicIPAddress);
			Debug.WriteLine(traceRouteResponses);
		}
	}
}
```
